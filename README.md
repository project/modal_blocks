CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Modal Blocks module displays a block in a modal on any page.

INSTALLATION
------------

1. Install and enable Modal Blocks module:
https://www.drupal.org/project/modal_blocks

2. Goto Structure > Block layout > place Modal block in any region.

3. View modal block on page.

CONFIGURATION
-------------

Configuration is strictly on the block level. No module configuration.

MAINTAINERS
-----------

**Current maintainer:**
* George Anderson (geoanders) - https://www.drupal.org/u/geoanders

**Past maintainers:**
 * Krishna Kanth (krknth) - https://www.drupal.org/u/krknth
 * Padma Priya Suriyaprakash (padma28) - https://www.drupal.org/u/padma28
 * Sarada Prasad (saradaprasad17) - https://www.drupal.org/u/saradaprasad17
 * MUNAVIJAYALAKSHMI P (Munavijayalakshmi) - https://www.drupal.org/u/munavijayalakshmi
