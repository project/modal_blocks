<?php

namespace Drupal\modal_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\block\Entity\Block;

/**
 * Provides a 'ModalBlock' block.
 *
 * @Block(
 *  id = "modal_block",
 *  admin_label = @Translation("Modal block"),
 * )
 */
class ModalBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['block'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Block to embed'),
      '#description' => $this->t('Select which block should be embedded inside modal block.'),
      '#target_type' => 'block',
      '#required' => TRUE,
    ];

    $form['period'] = [
      '#type' => 'select',
      '#title' => $this->t('Frequency period type'),
      '#description' => $this->t('Select which period type goes with the frequency value above.'),
      '#empty_option' => $this->t('- Select period type -'),
      '#options' => [
        'hour' => $this->t('Hour'),
        'day' => $this->t('Day'),
        'week' => $this->t('Week'),
        'month' => $this->t('Month'),
      ],
      '#default_value' => $config['period'] ?? 'day',
      '#required' => TRUE,
    ];

    $form['frequency'] = [
      '#type' => 'number',
      '#title' => $this->t('Frequency'),
      '#description' => $this->t('The frequency in which the modal block will appear. You can configure the frequency period below.'),
      '#min' => 0,
      '#default_value' => $config['frequency'] ?? '',
      '#required' => TRUE,
    ];

    $rand = 'modal-block-' . rand(100000, 999999);
    $form['random'] = [
      '#type' => 'hidden',
      '#value' => $config['random'] ?? $rand,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('block', $form_state->getValue('block'));
    $this->setConfigurationValue('frequency', $form_state->getValue('frequency'));
    $this->setConfigurationValue('period', $form_state->getValue('period'));
    $this->setConfigurationValue('random', $form_state->getValue('random'));
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('frequency'))) {
      if (!is_numeric($form_state->getValue('frequency'))) {
        $form_state->setErrorByName('frequency', $this->t('This field must be numeric.'));
      }
    }
    else {
      $form_state->setErrorByName('frequency', $this->t('Please fill in frequency.'));
    }
    if (empty($form_state->getValue('block'))) {
      $form_state->setErrorByName('block', $this->t('Please select block to embed.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->getConfiguration();
    $block_id = $config['block'];
    $frequency = $config['frequency'];
    $period = $config['period'];
    $random = $config['random'];
    $time = 0;

    switch ($period) {
      case 'hour':
        $time = 1 * 60 * 60 * 1000;
        break;
      case 'day':
        $time = 1 * 24 * 60 * 60 * 1000;
        break;
      case 'week':
        $time = 7 * 24 * 60 * 60 * 1000;
        break;
      case 'month':
        $time = 30 * 24 * 60 * 60 * 1000;
        break;
    }

    if ($block_id) {
      $block = Block::load($block_id);
      $block_content = \Drupal::entityTypeManager()
        ->getViewBuilder('block')
        ->view($block);
      $modal_block[] = [
        '#theme' => 'modal_block_formatter',
        '#block' => $block_content,
      ];
    }

    $modal_block['#attached']['drupalSettings']['modal_blocks']['frequency'] = $frequency;
    $modal_block['#attached']['drupalSettings']['modal_blocks']['period'] = $time;
    $modal_block['#attached']['drupalSettings']['modal_blocks']['random'] = $random;

    return $modal_block;
  }

}
